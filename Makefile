# handle the development/staging tasks

# CONFIGURATION
# mapped vagrant share
VAGRANT_ROOT=/var/www/nso

# useful WP paths
THEME_BASE=public/wp-content/themes/nso-nc
PLUGIN_BASE=public/wp-content/plugins
CONTENT_BASE=public/wp-content/uploads
WP_BASE=public

# mysql config
DB_NAME=nso
DB_USER=nso
DB_PASSWORD=1o3UvEY23YbweEFjCZZ6
DB_SNAPSHOT=nso.sql

STAGING_SSH=newcity@newcitystaging.com

# for Pipelines build
SSH=ssh -i /root/.ssh/pipelines_rsa
RSYNC=rsync -rlDvze '$(SSH)'

# TARGETS
default:
	@echo "Need a target. Pick one of these:"
	@# list the available targets (this cheats a bit)
	@grep '^[a-z]' Makefile | sed 's/:.*$$//' | sort

lint_theme:
	ls $(THEME_BASE)/**/*.php | xargs -L 1 php -l

checkvagrant:
	@# see if the VM is running
	@vagrant status | grep -q running

checkvpn:
	@# cute check to see if we're on the right network
	@ping -c1 -t1 192.168.6.100 > /dev/null

updatedb: pull_db_from_pantheon_live push_snapshot_to_dev

push_snapshot_to_dev: checkvagrant
	vagrant ssh -c 'mysql -u root -pclubsoda $(DB_NAME) < $(VAGRANT_ROOT)/$(DB_SNAPSHOT)'

dump_local_db: checkvagrant
	@# grab the dev db
	vagrant ssh -c 'mysqldump -u root -pclubsoda $(DB_NAME)' > $(DB_SNAPSHOT)
	rm -f ansible/roles/mysql/files/initial.sql
	ln -s $(DB_SNAPSHOT) ansible/roles/mysql/files/initial.sql

push_pl:
	rsync -rlDvz pl/public/ $(STAGING_SSH):/var/www/neu-nso/public/pl

.push_db_staging:
	scp $(DB_SNAPSHOT) $(STAGING_SSH):/tmp/
	ssh $(STAGING_SSH) "mysql -u $(DB_USER) -p$(DB_PASSWORD) $(DB_NAME) < /tmp/$(DB_SNAPSHOT)"

push_staging:
	$(RSYNC) public/ $(STAGING_SSH):/var/www/neu-nso/public

