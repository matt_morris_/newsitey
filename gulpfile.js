/* NewCity gulpfile behavior:
   default/dev  : CSS sourcemaps generated
	                Images unmodified

   --production : no sourcemaps
	                images optimized

   --clean      : deletes files not needed in production
	                (file list set in paths.clean_production variable)

   --jsmin      : minifies javascript files before copying

   --cssmin      : minifies CSS files before copying

   NOTE: Source folders are untouched during minification.
	     If gulp is run with an optimizing flag, then re-run without
		 a flag, the original files will be restored to the
		 destination folder.

*/

var gulp = require('gulp'),
	argv = require('yargs').argv,
	autoprefixer = require('gulp-autoprefixer'),
	browserSync = require('browser-sync'),
	cleanCSS = require('gulp-clean-css'),
	coffee = require('gulp-coffee'),
	colors = require('colors/safe'),
	concat = require('gulp-concat'),
	del = require('del'),
	exec = require('child_process').exec,
	gulpif = require('gulp-if'),
	gutil = require('gulp-util'),
	imagemin = require('gulp-imagemin'),
	modernizr = require('gulp-modernizr'),
	plumber = require('gulp-plumber'),
	pngquant = require('imagemin-pngquant'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	sanewatch = require('gulp-sane-watch'),
	svgmin = require('gulp-svgmin'),
	svgstore = require('gulp-svgstore'),
	inject = require('gulp-inject'),
	uglify = require('gulp-uglify');

// bower_path is used as a prefix in other paths
var bower_path = 'src/bower_components';

var paths = {
	patterns: ['src/patterns/**/*.twig', 'src/patterns/**/*.json', 'src/data/*.json'],
	sass: ['src/scss/**/*.scss', 'src/scss/**/*.sass'],
	clean_production: ['pl/source/css/style.css.map'],
	pl_css: 'pl/source/css',
	pl_css_public: 'pl/public/css',
	wp_css: 'theme/assets/css',
	pl_js: 'pl/source/js',
	pl_js_public: 'pl/public/js',
	wp_js: 'theme/assets/js',
	pl_images: 'pl/source/images',
	pl_images_public: 'pl/public/images',
	wp_images: 'theme/assets/images',
	pl_change_file: ['pl/public/latest-change.txt'],
	coffee: ['src/coffee/*.coffee'],
	js: ['src/js/**/*.js'],
	jquery: bower_path + '/jquery/dist/jquery.min.js',
	images: ['src/images/**/*'],
	svgstore: ['src/svgstore/**/*.svg'],
	vendorscripts: [
		// bower_path + '/jquery.fitvids/jquery.fitvids.js',
		// bower_path + '/slick-carousel/slick/slick.js',
		// bower_path + '/list.js/dist/list.js',

		// required for foundation
		bower_path + '/foundation-sites/js/foundation.core.js',
		bower_path + '/foundation-sites/js/foundation.util.mediaQuery.js',

		// optional utilities, needed based on the plugins below
		bower_path + '/foundation-sites/js/foundation.util.box.js',
		bower_path + '/foundation-sites/js/foundation.util.keyboard.js',
		bower_path + '/foundation-sites/js/foundation.util.mediaQuery.js',
		bower_path + '/foundation-sites/js/foundation.util.motion.js',
		bower_path + '/foundation-sites/js/foundation.util.nest.js',
//		bower_path + '/foundation-sites/js/foundation.util.timerAndImageLoader.js',
//		bower_path + '/foundation-sites/js/foundation.util.touch.js',
		bower_path + '/foundation-sites/js/foundation.util.triggers.js',

		// for individual plugins below, make sure to check the source for
		// utility function requirements
		//bower_path + '/foundation-sites/js/foundation.abide.js',
		// bower_path + '/foundation-sites/js/foundation.accordion.js',
		// bower_path + '/foundation-sites/js/foundation.accordionMenu.js',
		//bower_path + '/foundation-sites/js/foundation.drilldown.js',
		// bower_path + '/foundation-sites/js/foundation.dropdown.js',
		// bower_path + '/foundation-sites/js/foundation.dropdownMenu.js',
		bower_path + '/foundation-sites/js/foundation.equalizer.js',
		//bower_path + '/foundation-sites/js/foundation.interchange.js',
		//bower_path + '/foundation-sites/js/foundation.magellan.js',
		// bower_path + '/foundation-sites/js/foundation.offcanvas.js',
		//bower_path + '/foundation-sites/js/foundation.orbit.js',
		//bower_path + '/foundation-sites/js/foundation.resonsiveMenu.js',
		//bower_path + '/foundation-sites/js/foundation.resonsiveToggle.js',
		//bower_path + '/foundation-sites/js/foundation.reveal.js',
		//bower_path + '/foundation-sites/js/foundation.slider.js',
		// bower_path + '/foundation-sites/js/foundation.sticky.js',
		// bower_path + '/foundation-sites/js/foundation.tabs.js',
		bower_path + '/foundation-sites/js/foundation.toggler.js'
		//bower_path + '/foundation-sites/js/foundation.tooltip.js'
	]
};

// Error reporter for plumber.
var plumber_error = function (err) {
	gutil.log( gutil.colors.red('\u0007') );
	gutil.log( gutil.colors.red(err) );
	this.emit('end');
};

// Rebuild patternlab. This makes use of node's exec function instead of any
// of the gulp-specific solutions because it runs precisely once and does
// not depend on any vinyl objects.
var rebuild_pl = function(cb) {
	exec('php pl/core/console --generate',function(err,stdout,stderr) {
		console.log( colors.green(stdout) );
		console.log( colors.red(stderr) );
		cb(err);
	});
};

// application and third-party SASS -> CSS
gulp.task('styles', function() {
	return gulp.src( paths.sass )
		.pipe( plumber({ errorHandler: plumber_error }) )

		// If not in production mode, generate a sourcemap
		.pipe( gulpif( !argv.production, sourcemaps.init() ) )
			.pipe( sass({
				outputStyle: 'nested'
			 }) )
			.pipe( autoprefixer( [ 'last 2 versions', '> 1%' ] ) )
			// Minify css only if --cssmin flag is used
			.pipe( gulpif( argv.cssmin, cleanCSS() ) )
		.pipe(sourcemaps.write('.'))

		// Destination for the processed CSS file and sourcemap
		.pipe( gulp.dest( paths.pl_css ) )
		.pipe( gulp.dest( paths.wp_css ) )
		.pipe( gulp.dest( paths.pl_css_public ) )

		// Script to configure modernizr based on flags
		// that are actually used in the stylesheets
		.pipe( modernizr( 'modernizr-custom.js',{
			'options' : [
				'setClasses',
				"addTest",
				"html5printshiv",
				"testProp",
				"fnBind"
			]
		}) )
		// Uglify the modernizr script only if --jsmin is used
		.pipe( gulpif( argv.jsmin, uglify() ) )
		// Destinations for the custom modernizr rile
		.pipe( gulp.dest( paths.pl_js ) )
		.pipe( gulp.dest( paths.wp_js ) )
		.pipe( gulp.dest( paths.pl_js_public ) );
});

// Copy images from src to PL source destination
// (after optimizing them, if enabled)
gulp.task('images', function() {
	return gulp.src( paths.images )
		.pipe( plumber() )
		// Only optimize images if --production is used
		.pipe( gulpif( argv.production,
				imagemin( {
            		progressive: true,
            		use: [ pngquant() ]
					} ) ) )
		.pipe( gulp.dest( paths.pl_images ))
		.pipe( gulp.dest( paths.wp_images ))
		.pipe( gulp.dest( paths.pl_images_public ));
});

// just copy over jquery
// already injected in drupal by jquery_update
gulp.task('jquery', function() {
	return gulp.src( paths.jquery )
		.pipe( plumber() )
		.pipe( gulp.dest( paths.pl_js ) )
		.pipe( gulp.dest( paths.wp_js ) )
		.pipe( gulp.dest( paths.pl_js_public ) );
});

// application third-party JS -> combined JS
gulp.task('vendorscripts', function() {

	return gulp.src( paths.vendorscripts )
		.pipe( plumber() )
		// Uglify the javascript only if --jsmin is used
		.pipe( gulpif( argv.jsmin, uglify() ) )

		// concat and distribute static template scripts
		.pipe( concat( 'lib.js', { newLine: ';' } ) )
		.pipe( gulp.dest( paths.pl_js ) )
		.pipe( gulp.dest( paths.wp_js ) )
		.pipe( gulp.dest( paths.pl_js_public ) );
});

// Convert coffeescript to JavaScript and put it in the destination
// JS folder
gulp.task('coffee', function() {
	return gulp.src( paths.coffee )
		.pipe( plumber() )
		.pipe( coffee() )
		// Uglify the javascript only if --jsmin is used
		.pipe( gulpif( argv.jsmin, uglify() ) )
		.pipe( gulp.dest( paths.pl_js ) )
		.pipe( gulp.dest( paths.wp_js ) )
		.pipe( gulp.dest( paths.pl_js_public ) );
});

// Build the SVG spritesheet. This pulls everything
// out of the svgstore directory, combines them into
// one SVG element with the filename as an ID, then
// injects the result into a Twig macro definition.
// That macro is then called in a strategically
// selected template to ensure that the svgstore
// content is injected into the page template.

gulp.task('svgstore',function() {
	function fileContents(filePath,file) {
		return file.contents.toString();
	}

	svgs = gulp.src( paths.svgstore )
		.pipe( plumber() )
		.pipe( svgstore( { inlineSvg:true } ));

	return gulp
		.src('src/macros/svg.macro.twig')
		.pipe(inject(svgs, { transform:fileContents }))
		.pipe(gulp.dest('src/macros'));
});

// Copy javascript files over. These are for JS files not
// handled through bower or similar.
gulp.task('js',function() {
	return gulp.src(paths.js)
		.pipe( plumber() )
		// Uglify the javascript only if --jsmin is used
		.pipe( gulpif( argv.jsmin, uglify() ) )
		.pipe( gulp.dest( paths.pl_js ))
		.pipe( gulp.dest( paths.wp_js ))
		.pipe( gulp.dest( paths.pl_js_public ) );
});

/* Fonts should usually come from the CDN or a library (like font awesome)
 * rather than a file system unless we're building icon fonts ourselves.
 *
 *
gulp.task('fonts', function() {
	return gulp.src( paths.app.fonts )
		.pipe( plumber() )
		.pipe( gulp.dest( paths.dist.fonts ));
});
 */

// build-all builds everything in one go.
gulp.task('build-all', ['styles', 'jquery', 'vendorscripts', 'js', 'coffee', 'svgstore', 'images', 'clean'], rebuild_pl);

// Target just the patterns.
gulp.task('build-patterns', function() {
	rebuild_pl(function() {
		browserSync.reload();
	});
});

// All of our other typical parsers.
gulp.task('rebuild-styles', ['styles'], function() { browserSync.reload() });
gulp.task('rebuild-js', ['js'], function() { browserSync.reload() });
gulp.task('rebuild-coffee', ['coffee'], function() { browserSync.reload() });
gulp.task('rebuild-images', ['images'], function() { browserSync.reload() });
gulp.task('rebuild-svgstore', ['svgstore'], function() { gulp.start("build-patterns") });

// Deletes files not needed for production
// (should only be run in --production with --clean argument)
gulp.task('clean', function () {
	if (argv.clean && argv.production) {
		return del(paths.clean_production);
	} else {
		gutil.log(gutil.colors.yellow("Don't worry! Nothing was deleted\n           because you didn't use the --clean flag. "));
	}
});

gulp.task('reload', function() {
	browserSync.reload();
});

// all the watchy stuff
gulp.task('watcher', ['build-all'], function() {

	// sane is a more configurable watcher than gulp watch.
	// You can also have it use the more friendly OSX file
	// watcher "watchman", but that apparently has to be
	// installed by hand instead of through a dependency
	// manager.
	//
	// Installation instructions for Watchman:
	// https://facebook.github.io/watchman/docs/install.html
	//
	// Usage for gulp-sane-watch:
	// https://www.npmjs.com/package/gulp-sane-watch

	//var watcherOptions = { debounce:300,watchman:true };
	var watcherOptions = { debounce:300 };

	sanewatch(paths.patterns, watcherOptions,
		function() {
			gulp.start('build-patterns');
		}
	);

	sanewatch(paths.sass, watcherOptions,
		function() {
			gulp.start('rebuild-styles');
		}
	);

	sanewatch(paths.coffee, watcherOptions,
		function() {
			gulp.start('rebuild-coffee');
		}
	);

	sanewatch(paths.js, watcherOptions,
		function() {
			gulp.start('rebuild-js');
		}
	);

	sanewatch(paths.images, watcherOptions,
		function() {
			gulp.start('rebuild-images');
		}
	);

	sanewatch(paths.images, watcherOptions,
		function() {
			gulp.start('rebuild-images');
		}
	);

	// When patternlab rebuilds it modifies a text file
	// with the latest change information -- so we can
	// watch that one to see when to reload.
	// gulp.watch(paths.pl_change_file, ['reload']);

	browserSync.init({
		server: {
			baseDir: "./pl/public"
		}
	});
});

// Default build task
gulp.task('default', function() {
	gulp.start('watcher');
});

gulp.task('build', ['build-all']);
gulp.task('build-theme', ['styles', 'jquery', 'vendorscripts', 'js', 'coffee', 'svgstore', 'images', 'clean']);

