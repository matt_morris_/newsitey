$ = jQuery

$.fn.visible = (partial) ->
	###*
	# Copyright 2012, Digital Fusion
	# Licensed under the MIT license.
	# http://teamdf.com/jquery-plugins/license/
	#
	# @author Sam Sehnert
	# @desc A small plugin that checks whether elements are within
	#     the user visible viewport of a web browser.
	#     only accounts for vertical position, not horizontal.
	###

	$t = $(this)
	$w = $(window)
	viewTop = $w.scrollTop()
	viewBottom = viewTop + $w.height()
	_top = $t.offset().top
	_bottom = _top + $t.height()
	compareTop = if partial then _bottom else _top
	compareBottom = if partial then _top else _bottom
	
	compareBottom <= viewBottom and compareTop >= viewTop

initSlideIns = ->
	$slideIns = $('.slide-in')

	if $slideIns.length
		$slideIns.each ->
			$el = $(this)
			if $el.visible(true)
				$el.addClass 'already-visible'
			return

		$(window).scroll (event) ->
			$slideIns.each ->
				$el = $(this)
				if $el.visible(true)
					$el.addClass 'come-in'
				return
			return

$(document).foundation()
initSlideIns()