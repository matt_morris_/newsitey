<?php
/**
 * Template Name: Generic Page
 *
 * Template for displaying the home page
 *
 * @since 1.0.0
 */

get_header();


 // Context array
 $context         = Timber::get_context();
 $post            = new TimberPost();
 $context['post'] = $post;

 // Timber render().
 Timber::render( 'generic.twig', $context );
 ?>
