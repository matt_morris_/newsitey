<?php
/**
 * Template Name: Landing Page
 *
 * Template for displaying the home page
 *
 * @since 1.0.0
 */

get_header();


 // Context array
 $context         = Timber::get_context();
 $post            = new TimberPost();
 $context['post'] = $post;

 // Timber render().
 Timber::render( 'landing.twig', $context );
 ?>
