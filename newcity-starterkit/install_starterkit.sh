#!/bin/bash
# check if some dependencies are actually installed
# http://stackoverflow.com/questions/592620/check-if-a-program-exists-from-a-bash-script
command -v php >/dev/null 2>&1 || { echo >&2 "I require php but it's not installed."; exit 1; }
command -v composer >/dev/null 2>&1 || { echo >&2 "I require composer but it's not installed."; exit 1; }
command -v npm >/dev/null 2>&1 || { echo >&2 "I require npm but it's not installed."; exit 1; }

# where am I?
# http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR
submodule_dir=$( basename $DIR )
cd ..

###########################################
### Pattern lab and dependency installation

if [ ! -d pl ]; then
  echo "installing PL edition-twig-standard..."
  echo "when prompted to install a default starter kit, just press return (no install)"
  composer create-project pattern-lab/edition-twig-standard pl
else
  echo "updating composer dependencies..."
  cd pl
  composer update
  cd ..
fi

if [ ! -d pl/source ]; then
  echo "Can't find pl/source directory. Make sure I'm installed as a submodule in the base of your repo."
  exit 1
fi

cd pl
echo "setting up additional dependencies..."
composer require fzaninotto/faker:dev-master koenpunt/php-inflector
cd ..

###########################################



###########################################
### Source file linking or copying
if [ ! -d src ]; then

  echo
  read -n 1 -p "Install as editable base project (see README--will link src rather than copy it; y/N)? " answer
  case ${answer:0:1} in
    y|Y )
      echo
      echo "linking source files..."
      ln -s ${submodule_dir}/src .
      ln -s ${submodule_dir}/gulpfile.js .
      ln -s ${submodule_dir}/package.json .
    ;;
    * )
      echo
      echo "copying source files..."
      cp -R ${submodule_dir}/src src
      cp ${submodule_dir}/gulpfile.js .
      cp ${submodule_dir}/package.json .
    ;;
  esac

  cp ${submodule_dir}/.gitignore .

else
  # starterkit is already installed
  # just update things that are probably safe to update, but only do it if we were installed
  # as a client project (copied src, not linked)
  if [ ! -L src ]; then
    for d in macros twig-components; do
      rsync -av ${submodule_dir}/src/${d}/ src/${d}
    done
  fi
fi

# links between ./src and pl/source
cd pl/source
for d in annotations data layouts macros meta patterns twig-components; do
  if [ ! -L ./_${d} ]; then
    ln -s ../../src/${d} ./_${d}
  fi
done
cd ../..


###########################################



###########################################
### Install dev tools, finish up

if [ ! -d node_modules ]; then
  echo
  echo "installing node dependencies..."
  npm install
fi

echo
echo "running PL site generation..."
php pl/core/console --generate
