# NewCity PatternLab Starter Kit

Note that this isn't a true starter kit that may be installed by PL's built-in mechanism.  Instead, follow the steps below to get up and running with a fresh copy.

### 1. Set up your project

```
cd ~/sites  # or wherever you keep your work
mkdir NEW-CLIENT-PROJECT # whatever you want to call it
cd NEW-CLIENT-PROJECT
git init
```

From there there are two mechanisms for installing this kit and keeping it in sync upstream: as a git submodule, or using peru.  Submodules are git built-ins but can cause trouble occasionally.  Peru includes use a [python script](https://github.com/buildinspace/peru) but sync using a hidden folder, so the full starterkit may be included in your project repo.  Using peru will also make it harder to push changes you make to the starterkit back upstream.

You can also simply [download the zip of the repo](https://bitbucket.org/newcity/newcity-starterkit/downloads), unzip, and rename it `newcity-starterkit` in the root of your repo.  The disadvantage is it's harder to update later.

If you're working on an existing project you'll need to make sure you know which technique was used.  When you clone the repo, if the `newcity-starterkit` directory is empty, that's a pretty good sign the starterkit was installed as a submodule.

### 2a. Install this kit as a submodule

From the root of your NEW-CLIENT-PROJECT:

```
git submodule add git@bitbucket.org:newcity/newcity-starterkit.git
```

If you're starting work on someone else's existing project based on this kit, you need to initialize this submodule and update it to the commit where it's supposed to be in that repo. Hopefully the following will be included in that repo's README:

```
cd newcity-starterkit
git submodule init
git submodule update
cd ..
```

### 2b. Install with Peru

Make sure you have peru installed (`pip install peru`). From the root of your NEW-CLIENT-PROJECT

```
wget https://bitbucket.org/!api/2.0/snippets/newcity/KRMKz/d27557be74ee8edfcd8cda19312d0daa6e621baf/files/peru.yaml
peru sync
```

To later pull the starterkit up to the head of its repo:

```
peru reup newcity-starterkit
peru sync
```

### 3. Setup

```
newcity-starterkit/install_starterkit.sh
```

The above uses composer to bring in the PL base modules, creates source file links, and installs npm dependencies. You'll be prompted during installation to install a starter kit -- just press return to skip this.

You will also be prompted on whether you want to install an "editable base project".  If you're installing this project with the intent of working on or adding features to this base project specifically, answer `y`. If you're using the base project to start up a client project, you'll most likely want to answer `n`.

#### Huh?

When working on a client project, you probably don't want all your customizations to be reflected back upstream to the base project (this), which is why in this case the installer *copies* the source directory (i.e., the starter kit is purely a starting point). If you're working on this project itself, answering `y` above creates a *softlink* for the src folder, so any changes you make locally are changed in the submodule (this), and may then be committed to the repo as new feature branches.

### 4. Start it up

```
gulp
```

and edit the patterns and styles in `src/`