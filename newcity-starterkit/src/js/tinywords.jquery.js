;/*

Tinywords: wrap specified words in HTML content with a tag and class name so
they can get targeted typographic treatment. Safe to run on elements that
already contain HTML.

Options:
  words: an array of target words.
    Default: ['a','an','the','and','of','to']
  element: the HTML tag to create.
    Default: span
  className: the class name to use
    Default: 'tinywords'

*/

(function ( $ ) {

  $.fn.tinywords = function( options ) {

    var settings = $.extend({}, $.fn.tinywords.defaults, options);

    return this.each(function() {
      // Our regular expression for the words. Makes use of a negative
      // look-ahead expression to ensure we don't change text actually
      // inside an HTML tag (like a classname or attribute value).
      rx = new RegExp("\\b(" + settings.words.join('|') + ")(?![^<]*>)\\b",'gi');
      //" style='line-height: " + lineHeight + "'
      $(this).html($(this).html().replace(rx,"<" + settings.element + " class='" + settings.className + "'>$1</" + settings.element + ">"));
      return this;
    });

  };

  $.fn.tinywords.defaults = {
    words: ['a','an','the','and','of','to','is','in'],
    element: 'span',
    className: 'tinywords'
  }

}( jQuery ));
