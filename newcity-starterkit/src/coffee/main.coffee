$(document).foundation()

$(document).ready () ->
    $(".fit-media").fitVids()
    $(".auto-tinywords").tinywords()
    $(".fit-media").fitVids()
    $(".auto-tinywords").tinywords()
    $(".slideshow--feature").slick({
        dots: true
    })
    $(".slideshow--content").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true
    })
    listSortOptions = {
        valueNames: ['name'],
        listClass: "live-search-list",
        searchClass: "live-search-box"
    }
    sortableList = new List('live-search-wrapper', listSortOptions)
