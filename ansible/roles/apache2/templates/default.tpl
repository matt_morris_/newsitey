<VirtualHost *:80>

    #Names
    ServerAdmin webmaster@localhost
    Servername {{ domain }}
    #ServerAlias www.{{ domain }}
    DocumentRoot /var/www/{{ domain }}/public
    DirectoryIndex index.php index.html

    #Locations
    <Directory {{ doc_root }}>
            #BASE SETUP
            Options -Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            allow from all
    </Directory>

</VirtualHost>