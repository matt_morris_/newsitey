set daemon  120           # check services at 2-minute intervals

set mailserver localhost               # primary mailserver

set alert {{system_email}}                       # receive all alerts

set httpd port 2812 and
use address localhost
allow localhost     
     


###############################################################################
## Services
###############################################################################

#mysql
# add pid-file = /var/run/mysqld/mysqld.pid to my.cnf
check process mysql with pidfile /var/run/mysqld/mysqld.pid
group database
start program = "/etc/init.d/mysql start"
stop program = "/etc/init.d/mysql stop"
if failed host 127.0.0.1 port 3306 then restart
if 5 restarts within 5 cycles then timeout

#ssh
check process sshd with pidfile /var/run/sshd.pid
start program "/etc/init.d/ssh start"
stop program "/etc/init.d/ssh stop"
if failed port 22 protocol ssh then restarts
if 5 restarts within 5 cycles then timeout

#varnish
#check process varnish with pidfile /var/run/varnishd.pid
#start program = "/etc/init.d/varnish start"
#stop program = "/etc/init.d/varnish stop"
#if failed url http://localhost/
#then restart
#if 3 restarts within 5 cycles then timeout


#apache
check process apache2 with pidfile /var/run/apache2/apache2.pid
       start "/etc/init.d/apache2 start"
       stop  "/etc/init.d/apache2 stop"
	if failed url http://localhost:80/ with timeout 15 seconds
       then restart
#cron
check process crond with pidfile /var/run/crond.pid
start = "/etc/init.d/cron start"
stop  = "/etc/init.d/cron stop"#   depends on cron_rc

#postfix
#check process postfix with pidfile /var/spool/postfix/pid/master.pid
#group mail
#start program = "/etc/init.d/postfix start"
#stop  program = "/etc/init.d/postfix stop"
#if failed port 25 protocol smtp then restart
#if 5 restarts within 5 cycles then timeout

check filesystem sda1 with path /dev/xvda1
group server
if failed permission 660 then unmonitor
if failed uid root then unmonitor
if failed gid disk then unmonitor
if space usage > 80 % then alert
if inode usage > 80 % then alert